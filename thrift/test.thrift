service TestService {
  void sendSmallThing(
    1: i32 num,
    2: string doodad),
  
  void sendBigThing(
    1: i32 size,
    2: binary data)
}

service OtherService {
  void sendOtherSmallThing(
    1: i32 num,
    2: string doodad),

  void sendOtherBigThing(
    1: i32 size,
    2: binary data)
}