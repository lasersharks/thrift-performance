#include "TestService.h"
#include "OtherService.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransport.h>

#include <iostream>
#include <memory>
#include <thread>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

std::pair<std::shared_ptr<TestServiceClient>, std::shared_ptr<OtherServiceClient>>
createTestClient()
{
	std::shared_ptr<TTransport> transport(new TSocket("192.168.0.183", 9090));
	std::shared_ptr<TTransport> bufferedTransport(new TBufferedTransport(transport));
	bufferedTransport->open();

	std::shared_ptr<TProtocol> protocol(new TBinaryProtocol(bufferedTransport));

	std::shared_ptr<TProtocol> testProtocol(new TMultiplexedProtocol(protocol, "Test"));
	std::shared_ptr<TestServiceClient> client(new TestServiceClient(testProtocol));
	std::shared_ptr<TProtocol> otherTestProtocol(new TMultiplexedProtocol(protocol, "Other"));
	std::shared_ptr<OtherServiceClient> otherClient(new OtherServiceClient(otherTestProtocol));

	return std::make_pair(client, otherClient);
}

void runClientOne()
{
	auto client = createTestClient();
	auto test = client.first;
	auto other = client.second;

	//Send 200MB of 0s.
	std::vector<int8_t> twoHundredMb(1024 * 1024 * 1, 0);
	std::string twoHunderMbString(twoHundredMb.begin(), twoHundredMb.end());
	test->sendBigThing(1024 * 1024 * 1, twoHunderMbString);

	std::string smallThing = "Small Thing";
	other->sendOtherSmallThing(smallThing.size(), smallThing);
}

void runClientTwo()
{
	auto client = createTestClient();
	auto test = client.first;
	auto other = client.second;

	for (uint32_t i = 0; i < 10; i++)
	{
		std::string smallThing = "Small Thing";
		test->sendSmallThing(smallThing.size(), smallThing);
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

int main(int argc, char** argv)
{
	std::thread t1(runClientOne);
	std::thread t2(runClientTwo);

	t1.join();
	t2.join();
}