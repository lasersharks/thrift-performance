#include "TestService.h"
#include "OtherService.h"
#include <thrift/processor/TMultiplexedProcessor.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

#include <iostream>
#include <memory>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

class TestServiceHandler : virtual public TestServiceIf
{
public:
	TestServiceHandler() 
	{
		// Your initialization goes here
	}

	// Inherited via TestServiceIf
	virtual void sendSmallThing(const int32_t i, const std::string & s) override
	{
		std::cout << "Received small thing: " << s << std::endl;
	}

	virtual void sendBigThing(const int32_t size, const std::string & data) override
	{
		std::cout << "Received big thing of size: " << size << std::endl;
	}

};

class OtherServiceHandler : virtual public OtherServiceIf
{
public:
	OtherServiceHandler()
	{

	}
	// Inherited via OtherServiceIf
	virtual void sendOtherSmallThing(const int32_t num, const std::string & doodad) override
	{
		std::cout << "Received other small thing: " << doodad << std::endl;
	}

	virtual void sendOtherBigThing(const int32_t size, const std::string & data) override
	{
		std::cout << "Received other big thing of size: " << size << std::endl;
	}
};

int main(int argc, char **argv) {
	int port = 9090;

	std::shared_ptr<TMultiplexedProcessor> processor(new TMultiplexedProcessor());

	std::shared_ptr<TestServiceHandler> handler(new TestServiceHandler());
	std::shared_ptr<TestServiceProcessor> testProcessor(new TestServiceProcessor(handler));
	processor->registerProcessor("Test", testProcessor);

	std::shared_ptr<OtherServiceHandler> otherHandler(new OtherServiceHandler());
	std::shared_ptr<OtherServiceProcessor> otherProcessor(new OtherServiceProcessor(otherHandler));
	processor->registerProcessor("Other", otherProcessor);

	std::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
	std::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
	std::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

	TThreadedServer server(processor, serverTransport, transportFactory, protocolFactory);
	server.serve();
	return 0;
}

